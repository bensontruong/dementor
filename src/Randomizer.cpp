#include "Randomizer.h"

#include <stdlib.h>
#include <time.h>

Randomizer::Randomizer(int count) :
    m_count(count)
{
}

Randomizer::~Randomizer()
{
}

int Randomizer::GetRandomNumber() const
{
    srand(time(NULL));
    return rand() % m_count;
}

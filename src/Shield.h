//******************************************************************************
// Copyright © 2016 Zodiac Inflight Innovations, a subsidiary of Zodiac
// Aerospace.  All rights reserved.  The contents of this medium are
// confidential and proprietary to Zodiac Aerospace, and shall not be
// disclosed, disseminated, copied or used except as expressly authorized in
// writing by Zodiac Inflight Innovations.
//******************************************************************************
#ifndef SHIELD_H_
#define SHIELD_H_

#include "Subsystem.h"

class Shield : public Subsystem {
public:
    Shield ();
    virtual ~Shield ();
    bool IsRaised();
    void RaiseShield();

    bool IsBuckled();

    void IncreasePower(int amount);
    void Hit(int amount);
    int GetUnabsorbedEnergy();

private:
    bool m_isRaised;
    bool m_isBuckled;
    int  m_unabsorbedEnergy;

};

#endif /* SHIELD_H_ */

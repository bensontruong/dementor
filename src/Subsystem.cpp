#include "Subsystem.h"

Subsystem::Subsystem() :
    m_energy(0),
    m_isDamaged(false),
    m_name("")
{
}

Subsystem::Subsystem(int initialEnergy) :
    m_energy(initialEnergy),
    m_isDamaged(false)
{
}

Subsystem::~Subsystem()
{
}

Subsystem::Subsystem(const Subsystem& other)
{
    m_energy = other.m_energy;
    m_isDamaged = other.m_isDamaged;
    m_name = other.m_name;
}

Subsystem& Subsystem::operator=(const Subsystem& other)
{
    m_energy = other.m_energy;
    m_isDamaged = other.m_isDamaged;
    m_name = other.m_name;

    return *this;
}

void Subsystem::SetName(const std::string& name)
{
    m_name = name;
}

std::string Subsystem::Name() const
{
    return m_name;
}

int Subsystem::GetEnergy() const
{
    return m_energy;
}

bool
Subsystem::IsDamaged() const
{
    return m_isDamaged;
}

void Subsystem::SetDamaged()
{
    m_isDamaged = true;
}

void Subsystem::Repair()
{
    m_isDamaged = false;
}

#ifndef RANDOMIZER_H
#define RANDOMIZER_H

class Randomizer
{
public:
    Randomizer(int count);
    virtual ~Randomizer();

    int GetRandomNumber() const;

private:
    const int m_count;
};

#endif // RANDOMIZER_H

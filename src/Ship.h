#ifndef SHIP_H
#define SHIP_H

#include <vector>
#include "Randomizer.h"
#include "Shield.h"
#include "Subsystem.h"

class Ship
{
public:
    Ship();
    Ship(const Randomizer& randomizer);
    ~Ship();

    bool TransferPower(int amount);
    void RaiseShield();
    void TakeHit(int amount);

    typedef std::vector<Subsystem> Subsystems;
    Subsystems GetDamagedSubsystems() const;
    int GetPower();

private:
    Subsystem& PickRandomSubsystem();

    int m_power;

    // subsystems
    Shield m_shield;
    Subsystem m_weapon;
    Subsystem m_engines;
    Randomizer m_randomizer;
};

#endif // SHIP_H

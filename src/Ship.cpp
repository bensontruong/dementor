#include <stdlib.h>
#include <time.h>
#include <cstddef>
#include "Ship.h"

namespace
{
const int INITIAL_POWER = 100000;
};

Ship::Ship(const Randomizer& randomizer) :
    m_power(INITIAL_POWER),
    m_randomizer(randomizer)
{
    m_shield.SetName("shield");
    m_engines.SetName("engines");
    m_weapon.SetName("weapon");
}

Ship::Ship() :
    m_power(INITIAL_POWER),
    m_randomizer(3) // smelly
{
}

Ship::~Ship()
{
}

bool Ship::TransferPower(int amount)
{
    if (amount <= m_power)
    {
        int current = m_shield.GetEnergy();
        m_shield.IncreasePower(amount);

        m_power -= m_shield.GetEnergy() - current;
        return true;
    }

    return false;
}

void Ship::RaiseShield()
{
    m_shield.RaiseShield();
}

void Ship::TakeHit(int amount)
{
    if (m_shield.IsRaised())
    {
        m_shield.Hit(amount);
    }

    if (m_shield.GetEnergy() <= 0)
    {
        Subsystem& subsystemToDamage = PickRandomSubsystem();
        subsystemToDamage.SetDamaged();
    }
}

Subsystem& Ship::PickRandomSubsystem()
{
    int randSubsystemIndex = m_randomizer.GetRandomNumber();
#if 0
    srand(time(NULL));
    int randSubsystemIndex = rand() % 3;
#endif

    switch (randSubsystemIndex)
    {
    case 0:
    default:
        return m_shield;
    
    case 1:
        return m_weapon;

    case 2:
        return m_engines;
    }
}

Ship::Subsystems Ship::GetDamagedSubsystems() const
{
    Subsystems damagedSubsystems;

    if (m_shield.IsDamaged())
    {
        damagedSubsystems.push_back(m_shield);
    }

    if (m_weapon.IsDamaged())
    {
        damagedSubsystems.push_back(m_weapon);
    }

    if (m_engines.IsDamaged())
    {
        damagedSubsystems.push_back(m_engines);
    }

    return damagedSubsystems;
}

int
Ship::GetPower() {
    return m_power;
}

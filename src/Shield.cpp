//******************************************************************************
// Copyright © 2016 Zodiac Inflight Innovations, a subsidiary of Zodiac
// Aerospace.  All rights reserved.  The contents of this medium are
// confidential and proprietary to Zodiac Aerospace, and shall not be
// disclosed, disseminated, copied or used except as expressly authorized in
// writing by Zodiac Inflight Innovations.
//******************************************************************************
#include "Shield.h"

namespace {
    const int MAX_ENERGY = 10000;
}

Shield::Shield () : m_isRaised(false), m_isBuckled(false), m_unabsorbedEnergy(0) {
}

Shield::~Shield () {
}

bool
Shield::IsRaised() {
    return m_isRaised;
}

void
Shield::RaiseShield() {
    if(!m_isBuckled && !m_isDamaged && m_energy > 0) {
        m_isRaised = true;
    }
}

bool
Shield::IsBuckled() {
    return m_isBuckled;
}

void
Shield::IncreasePower(int amount) {

    if(amount <= 0) {
        return;
    }

    if(m_energy + amount > MAX_ENERGY) {
        m_energy = MAX_ENERGY;
    } else {
        m_energy += amount;
    }

    m_isBuckled = false;
    m_unabsorbedEnergy = 0;
}

void
Shield::Hit(int amount){

    if(amount >= m_energy) {
        m_isBuckled = true;
        m_isRaised = false;
        m_unabsorbedEnergy = m_energy - amount;
        m_energy = 0;
    } else {
        m_energy -= amount;
    }
}

int
Shield::GetUnabsorbedEnergy(){
    return m_unabsorbedEnergy;
}

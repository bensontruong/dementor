#ifndef SUBSYSTEM_H
#define SUBSYSTEM_H

#include <string>

class Subsystem
{
public:
    Subsystem();
    Subsystem(int initialEnergy);

    Subsystem(const Subsystem& other);
    Subsystem& operator=(const Subsystem& other);

    virtual ~Subsystem();

    void SetName(const std::string& name);
    std::string Name() const;
    int GetEnergy() const;
    bool IsDamaged() const;
    void SetDamaged();
    void Repair();

protected:
    int m_energy;
    bool m_isDamaged;
    std::string m_name;
};

#endif // SUBSYSTEM_H

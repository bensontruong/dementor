#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "Ship.h"

class ShipTest : public ::testing::Test
{
public:
    ShipTest()
    {
    }

    ~ShipTest()
    {
    }
};

TEST_F(ShipTest, GetDamagedSubsystems_Empty)
{
    Ship enterprise;
    EXPECT_EQ(static_cast<size_t>(0), enterprise.GetDamagedSubsystems().size());
}

TEST_F(ShipTest, TakeHit)
{
    Ship enterprise;
    enterprise.TakeHit(10001);
    const Ship::Subsystems& damagedSubsystems = enterprise.GetDamagedSubsystems();
    EXPECT_EQ(static_cast<size_t>(1), damagedSubsystems.size());
}

TEST_F(ShipTest, RandomSubsystem)
{
    Randomizer randomizer(1);
    Ship enterprise(randomizer);

    enterprise.TakeHit(10001);
    const Ship::Subsystems& damagedSubsystems = enterprise.GetDamagedSubsystems();

    ASSERT_TRUE(damagedSubsystems.size() == 1);
    EXPECT_EQ("shield", damagedSubsystems[0].Name());
}

TEST_F(ShipTest, GetInitialPower)
{
    Ship enterprise;
    EXPECT_EQ(100000, enterprise.GetPower());
}

TEST_F(ShipTest, TransferPowerFromReserve)
{
    Ship enterprise;

    EXPECT_TRUE(enterprise.TransferPower(10000));
    EXPECT_EQ(90000, enterprise.GetPower());
}

TEST_F(ShipTest, TransferMorePowerThanReserve)
{
    Ship enterprise;

    EXPECT_FALSE(enterprise.TransferPower(100001));
    EXPECT_EQ(100000, enterprise.GetPower());
}

TEST_F(ShipTest, TransferTooMuchEnergyToShield)
{
    Ship enterprise;

    EXPECT_TRUE(enterprise.TransferPower(10001));
    EXPECT_EQ(90000, enterprise.GetPower());
}

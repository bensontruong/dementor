//******************************************************************************
// Copyright © 2016 Zodiac Inflight Innovations, a subsidiary of Zodiac
// Aerospace.  All rights reserved.  The contents of this medium are
// confidential and proprietary to Zodiac Aerospace, and shall not be
// disclosed, disseminated, copied or used except as expressly authorized in
// writing by Zodiac Inflight Innovations.
//******************************************************************************
#include "Shield.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

class ShieldTestFixture : public ::testing::Test
{
public:
    ShieldTestFixture(){}

    ~ShieldTestFixture(){}
protected:
    virtual void SetUp(){

    }
    virtual void TearDown(){
    }

};

TEST_F(ShieldTestFixture, Shield_IsRaised){
    Shield shield;
    EXPECT_FALSE(shield.IsRaised());
}

TEST_F(ShieldTestFixture, Shield_ShieldsNotRaisedIfNoPower){
    Shield shield;
    shield.RaiseShield();
    EXPECT_FALSE(shield.IsRaised());
}

TEST_F(ShieldTestFixture, Shield_ShieldsRaised){
    Shield shield;
    shield.IncreasePower(5);
    shield.RaiseShield();
    EXPECT_TRUE(shield.IsRaised());
}

TEST_F(ShieldTestFixture, Shield_StartWithZeroEnergy){
    Shield shield;
    EXPECT_EQ(shield.GetEnergy(), 0);
}

TEST_F(ShieldTestFixture, Shield_IncreasePowerFromZero){
    Shield shield;
    shield.IncreasePower(5);
    EXPECT_EQ(shield.GetEnergy(), 5);
}

TEST_F(ShieldTestFixture, Shield_MaxEnergyFromZero){
    Shield shield;
    shield.IncreasePower(10000);
    EXPECT_EQ(shield.GetEnergy(), 10000);
}

TEST_F(ShieldTestFixture, Shield_IncreaseZeroEnergyDoesNotImpactShield){
    Shield shield;
    shield.IncreasePower(0);
    EXPECT_EQ(shield.GetEnergy(), 0);

    EXPECT_FALSE(shield.IsBuckled());
    EXPECT_EQ(0, shield.GetUnabsorbedEnergy());
}

TEST_F(ShieldTestFixture, Shield_IncreaseZeroEnergyDoesNotImpactBuckledShield){
    Shield shield;
    shield.RaiseShield();
    shield.IncreasePower(5);
    shield.Hit(5);
    EXPECT_TRUE(shield.IsBuckled());
    EXPECT_FALSE(shield.IsRaised());

    shield.IncreasePower(0);
    EXPECT_TRUE(shield.IsBuckled());
}

TEST_F(ShieldTestFixture, Shield_IncreaseZeroEnergyDoesNotImpactUnabsorbedEnergy){
    Shield shield;
    shield.RaiseShield();
    shield.IncreasePower(5);
    shield.Hit(6);
    EXPECT_TRUE(shield.IsBuckled());
    EXPECT_FALSE(shield.IsRaised());
    EXPECT_EQ(-1, shield.GetUnabsorbedEnergy());

    shield.IncreasePower(0);
    EXPECT_EQ(-1, shield.GetUnabsorbedEnergy());
}

TEST_F(ShieldTestFixture, Shield_OverMaxEnergy){
    Shield shield;
    shield.IncreasePower(10001);
    EXPECT_EQ(shield.GetEnergy(), 10000);
}

TEST_F(ShieldTestFixture, Shield_HitShield){
    Shield shield;
    shield.RaiseShield();
    shield.IncreasePower(5);
    shield.Hit(5);
    EXPECT_EQ(shield.GetEnergy(), 0);
}

TEST_F(ShieldTestFixture, Shield_HitShieldMinimumEnergy){
    Shield shield;
    shield.RaiseShield();
    shield.IncreasePower(5);
    shield.Hit(6);
    EXPECT_EQ(shield.GetEnergy(), 0);
}

TEST_F(ShieldTestFixture, Shield_IsBuckled){
    Shield shield;
    EXPECT_FALSE(shield.IsBuckled());
}

TEST_F(ShieldTestFixture, Shield_BecomesBuckled){
    Shield shield;
    shield.RaiseShield();
    shield.IncreasePower(5);
    shield.Hit(5);
    EXPECT_TRUE(shield.IsBuckled());
    EXPECT_FALSE(shield.IsRaised());
}

TEST_F(ShieldTestFixture, Shield_CannotBeRaisedWhenBuckled){
    Shield shield;
    shield.RaiseShield();
    shield.IncreasePower(5);
    shield.Hit(5);
    EXPECT_TRUE(shield.IsBuckled());
    EXPECT_FALSE(shield.IsRaised());

    shield.RaiseShield();
    EXPECT_TRUE(shield.IsBuckled());
    EXPECT_FALSE(shield.IsRaised());
}

TEST_F(ShieldTestFixture, Shield_BecomesUnbuckled){
    Shield shield;
    shield.RaiseShield();
    shield.IncreasePower(5);
    shield.Hit(5);
    EXPECT_TRUE(shield.IsBuckled());

    shield.IncreasePower(5);
    shield.RaiseShield();

    EXPECT_FALSE(shield.IsBuckled());
    EXPECT_TRUE(shield.IsRaised());
    EXPECT_EQ(5, shield.GetEnergy());
}

TEST_F(ShieldTestFixture, Shield_StaysUnbuckled){
    Shield shield;
    shield.RaiseShield();
    shield.IncreasePower(5);
    shield.Hit(5);
    EXPECT_TRUE(shield.IsBuckled());

    shield.RaiseShield();
    EXPECT_FALSE(shield.IsRaised());

    shield.IncreasePower(5);
    EXPECT_FALSE(shield.IsBuckled());
    EXPECT_FALSE(shield.IsRaised());
}

TEST_F(ShieldTestFixture, Shield_GetUnabsorbedEnergyInitial){
    Shield shield;
    EXPECT_EQ(shield.GetUnabsorbedEnergy(), 0);
}

TEST_F(ShieldTestFixture, Shield_GetUnabsorbedEnergyAfterHitGoesNeutral){
    Shield shield;
    shield.RaiseShield();
    shield.IncreasePower(5);
    shield.Hit(5);
    EXPECT_EQ(shield.GetUnabsorbedEnergy(), 0);
}

TEST_F(ShieldTestFixture, Shield_GetUnabsorbedEnergyAfterHitGoesNegative){
    Shield shield;
    shield.RaiseShield();
    shield.IncreasePower(5);
    shield.Hit(6);
    EXPECT_EQ(shield.GetUnabsorbedEnergy(), -1);
}

TEST_F(ShieldTestFixture, Shield_ShieldAbsorbsEnergy){
    Shield shield;
    shield.RaiseShield();
    shield.IncreasePower(5);
    shield.Hit(4);
    EXPECT_EQ(shield.GetUnabsorbedEnergy(), 0);
}

TEST_F(ShieldTestFixture, Shield_IncreasePowerResetsUnabsorbedEnergy){
    Shield shield;
    shield.RaiseShield();
    shield.IncreasePower(5);
    shield.Hit(6);
    EXPECT_EQ(shield.GetUnabsorbedEnergy(), -1);

    shield.IncreasePower(1);
    EXPECT_EQ(shield.GetUnabsorbedEnergy(), 0);
}

TEST_F(ShieldTestFixture, Shield_CannotRaiseWhenDamaged){
    Shield shield;
    shield.SetDamaged();

    shield.RaiseShield();
    EXPECT_FALSE(shield.IsRaised());
}

TEST_F(ShieldTestFixture, Shield_RaiseAfterRepaired){
    Shield shield;
    shield.SetDamaged();

    shield.RaiseShield();
    EXPECT_FALSE(shield.IsRaised());

    shield.Repair();
    shield.IncreasePower(5);
    shield.RaiseShield();
    EXPECT_TRUE(shield.IsRaised());
}

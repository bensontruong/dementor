
#include "gtest/gtest.h"
#include "gmock/gmock.h"


int main(int argc, char** argv)
{
	// run all tests
	::testing::InitGoogleMock(&argc, argv);

	return RUN_ALL_TESTS();
}

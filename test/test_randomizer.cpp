#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "Randomizer.h"

class RandomizerTest : public ::testing::Test
{
public:
    RandomizerTest()
    {
    }

    ~RandomizerTest()
    {
    }
};

// Intent: verify more than one random number is generated 
// based on a range of 10 values.
TEST_F(RandomizerTest, GetRandomNumber)
{
    Randomizer rnd(10);
    int results[10];

    for (int i = 0; i < 10; ++i)
    {
        const int result = rnd.GetRandomNumber();
        ASSERT_TRUE(result >= 0 && result < 10);
        ++results[result];
    }

    int numNonZeroResults = 0;
    for (int i = 0; i < 10; ++i)
    {
        if (results[i] > 0)
        {
            ++numNonZeroResults;
        }
    }

    ASSERT_TRUE(numNonZeroResults > 0);
}

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "Subsystem.h"

class SubsystemTest : public ::testing::Test
{
public:
    SubsystemTest()
    {
    }

    ~SubsystemTest()
    {
    }
};


TEST_F(SubsystemTest, IsDamaged)
{
    Subsystem ss;
    EXPECT_FALSE(ss.IsDamaged());
}

TEST_F(SubsystemTest, SetDamaged)
{
    Subsystem ss;
    ss.SetDamaged();
    EXPECT_TRUE(ss.IsDamaged());
}

TEST_F(SubsystemTest, Repair)
{
    Subsystem ss;
    ss.SetDamaged();
    ss.Repair();
    EXPECT_FALSE(ss.IsDamaged());
}

CXXFLAGS = -g -Wall -Werror -pthread -std=c++0x

UNAME_I := $(shell uname -i)
ifeq ($(UNAME_I), x86_64)
	LFLAGS = -L../../lib
	INCLUDES = -I../../inc -I. -I../src
else
	LFLAGS = -Llib
	INCLUDES = -Iinc -I. -I../src
endif

LIBS = -l:gmock_main.a -lpthread
PRGM  = DementorApp
SRCS := $(wildcard src/*.cpp)
OBJS := $(SRCS:.cpp=.o)
DEPS := $(OBJS:.o=.d)

.PHONY: all clean test

all: $(PRGM) test

test:
	make -C test

$(PRGM): $(OBJS)
	$(CXX) $(OBJS) $(LFLAGS) $(LIBS) -o $@

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDES) -MMD -MP -c $< -o $@

clean:
	make -C test clean
	rm -rf $(OBJS) $(DEPS) $(PRGM)

-include $(DEPS)
